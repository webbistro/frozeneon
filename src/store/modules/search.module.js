import baseAxios from '../../api'

export default {
  namespaced: true,
  state () {
    return {
      lists: []
    }
  },
  mutations: {
    setPackage (state, lists) {
      state.lists = lists
    }
  },
  actions: {
    async load ({ commit }, text, size = 250) {
      const { data } = await baseAxios.get(`/-/v1/search?text=${text}&size=${size}`)
      commit('setPackage', data)
    }
  },
  getters: {
    lists: (state) => state.lists
  }
}
