import { createStore } from 'vuex'
import search from './modules/search.module'

export default createStore({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    search
  }
})
