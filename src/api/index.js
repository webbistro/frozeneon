import axios from 'axios'

const baseAxios = axios.create({
  baseURL: process.env.VUE_APP_API
})

baseAxios.defaults.params = {}

baseAxios.interceptors.response.use(null, (error) => {
  return Promise.reject(error)
})

export default baseAxios
